import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './component/app/app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BioRapideComponent } from './bio-rapide/bio-rapide.component';
import { MesProjetsComponent } from './mes-projets/mes-projets.component';
import { GianniBComponent } from './gianni-b/gianni-b.component';
import { MesDiplomeComponent } from './mes-diplome/mes-diplome.component';
import { MeContacterComponent } from './me-contacter/me-contacter.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BioRapideComponent,
    MesProjetsComponent,
    GianniBComponent,
    MesDiplomeComponent,
    MeContacterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
