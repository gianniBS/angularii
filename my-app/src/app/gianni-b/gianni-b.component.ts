import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gianni-b',
  templateUrl: './gianni-b.component.html',
  styleUrls: ['./gianni-b.component.scss']
})
export class GianniBComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  getConfirmation()
  {
    let retVal = confirm("Voulez-vous vraiment voir le code ?");
    if (retVal == true)
    {
      window.location.href = 'https://gitlab.com/gianniBS/html';
      return true;
    }
    else
    {
      return false;
    }
  }
}
