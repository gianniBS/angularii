import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GianniBComponent } from './gianni-b.component';

describe('GianniBComponent', () => {
  let component: GianniBComponent;
  let fixture: ComponentFixture<GianniBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GianniBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GianniBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
