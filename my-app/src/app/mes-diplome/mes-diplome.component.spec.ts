import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesDiplomeComponent } from './mes-diplome.component';

describe('MesDiplomeComponent', () => {
  let component: MesDiplomeComponent;
  let fixture: ComponentFixture<MesDiplomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesDiplomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MesDiplomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
