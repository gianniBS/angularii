import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BioRapideComponent} from "./bio-rapide/bio-rapide.component";
import {MesProjetsComponent} from "./mes-projets/mes-projets.component";
import {GianniBComponent} from "./gianni-b/gianni-b.component";
import {MesDiplomeComponent} from "./mes-diplome/mes-diplome.component";
import {MeContacterComponent} from "./me-contacter/me-contacter.component";

const routes: Routes = [{
  path:"bio-rapide", component:BioRapideComponent
},{
  path:"mes-projets", component:MesProjetsComponent
},{
  path:"giannib", component:GianniBComponent
},{
  path:"mes-diplomes", component:MesDiplomeComponent
},{
  path:"me-contacter", component:MeContacterComponent
},{
  path:"", component:GianniBComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
