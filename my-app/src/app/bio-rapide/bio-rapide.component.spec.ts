import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BioRapideComponent } from './bio-rapide.component';

describe('BioRapideComponent', () => {
  let component: BioRapideComponent;
  let fixture: ComponentFixture<BioRapideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BioRapideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BioRapideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
