import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mes-projets',
  templateUrl: './mes-projets.component.html',
  styleUrls: ['./mes-projets.component.scss']
})
export class MesProjetsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  getConfirmation()
  {
    let retVal = confirm("Voulez-vous vraiment voir le code ?");
    if (retVal == true)
    {
      window.location.href = 'https://gitlab.com/gianniBS/html';
      return true;
    }
    else
    {
      return false;
    }
  }
}
